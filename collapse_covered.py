def collapse_helper(c_set):
    for (i,j) in c_set:
        # combine covered areas
        for (m,n) in [(m,n) for (m, n) in c_set if m >= i and m < j and n > j]:
            c_set.add((i, n))
            c_set.remove((i,j))
            c_set.remove((m,n))
            return c_set, True
            
        # deal with reads that are subsets of other reads
        for (m,n) in [(m,n) for (m, n) in c_set if m > i and m < j and n < j]:
            c_set.remove((m, n))
            return c_set, True
            
    return c_set, False

def collapse(c):
    c_set = set(c)
    print("Pre collapse", c_set)
    keep_going = 1
    while(keep_going):
        c_set, keep_going = collapse_helper(c_set)
    print("Post collapse", c_set)
    return c_set

# https://en.wikibooks.org/wiki/Algorithm_Implementation/Strings/Longest_common_substring#Python_3
# very inefficient, but gives the right answer
def longest_common_substring(s1, s2):
    m = [[0] * (1 + len(s2)) for i in range(1 + len(s1))]
    longest, x_longest = 0, 0
    for x in range(1, 1 + len(s1)):
        for y in range(1, 1 + len(s2)):
            if s1[x - 1] == s2[y - 1]:
                m[x][y] = m[x - 1][y - 1] + 1
                if m[x][y] > longest:
                    longest = m[x][y]
                    x_longest = x
            else:
                m[x][y] = 0
    return s1[x_longest - longest: x_longest]
    
#collapse([(397, 1347), (2994, 3920), (3201, 3971), (2493, 3354), (898, 1880), (1376, 2284), (2001, 2896)])