import os
import shutil
import time
import zipfile
import re

from Bio import SeqIO
import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm

from mafft_align import mafft_align
from benchling_cached_fetch import benchling_cached_fetch
from collapse_covered import collapse, longest_common_substring

def trim_benchling_search(x):
    '''Trims off a : and a _ or - off of the sample name, leaving just the plasmid name'''
    i = x.find(':')
    if i == -1:
        i = 0
    j = x.find('_')
    if j == -1:
        j = x.find('-')
    return x[i:j]

def clean_up_folders():
    # empties extracted folder
    if os.path.isdir("extracted"):
        shutil.rmtree("extracted")
    os.mkdir("extracted")

    # renames samples folder
    if os.path.isdir("samples"):
        os.rename("samples", "samples_before_" + str(time.time()))
    os.mkdir("samples")

def extract_new_files():
    # if there are any new zip files, extract them to the extracted directory
    for file in os.listdir("./attachments"):
        if file.endswith(".zip"):
            p = os.path.join("./attachments", file)

            #regex to parse folder name

            zip_ref = zipfile.ZipFile(p, 'r')
            zip_ref.extractall("./extracted")
            zip_ref.close()

    # if there are new files in the extracted directory, delete .seq and move ab1 files to
    # their own folders
    for file in os.listdir("./extracted"):
        p = os.path.join("./extracted", file)

        # parse the sample name out of the .ab1 filename and move to its own directory
        if file.endswith(".ab1"):
            # (\d\d\D)_(\d*)_(\D\d{1,5})-(.{1,100})-(\D\d\d)_(\D\D\d{1,4})-(\D\d\d)_(\D\d\d).ab1
            # \d\d\D_\d*_(.*)-\D\d\d_\D\D\d{1,4}-\D\d\d_\D\d\d.ab1
            # https://pythex.org/
            #m = re.search("\d\d\D_\d*_(.*)-\D\d\d_\D\D\d{1,4}-\D\d\d_\D\d\d.ab1", p)
            m = re.search(r"\d\d\D_\d*_(.*)-\D\d\d_\D\D\d{1,4}", p)
            if m:
                sample = m.groups()[0]
                sample_path = os.path.join("./samples", sample)
                if not os.path.exists(sample_path):
                    os.makedirs(sample_path)
                os.rename(p, os.path.join(sample_path, file))
            else:
                print("regex not found", file)

def fetch_from_benchling(prefetch=False):
    '''Dowloads the maps from benchling.'''
    sample_names = []
    if prefetch:
        # For fetching samples from a list, before you have sequencing results
        with open('prefetch.txt') as f:
            sample_names = f.readlines()
            sample_names = [x.strip() for x in sample_names]
    else:
        # Fetch samples based on sequencing results you've already recieved
        sample_names = [x for x in os.listdir("./samples") if x.startswith('.') is False]

    for s in sample_names:
        s = trim_benchling_search(s)
        print("Fetching reference sequence from Benchling for ", s)
        benchling_cached_fetch(s)

def arrange_benchling_files():
    '''Dowloads the maps from benchling.'''
    # Fetch samples based on sequencing results you've already recieved
    sample_names = [x for x in os.listdir("./samples") if x.startswith('.') is False]

    # make a folder for each sample
    for s in sample_names:
        sample_path = os.path.join("./samples", s)
        if not os.path.exists(sample_path):
            os.makedirs(sample_path)

    # download each map
    sample_folders = [x for x in os.listdir("./samples") if x.startswith('.') is False]
    for folder in sample_folders:
        files = os.listdir(os.path.join("./samples", folder))

        if all(f.startswith('ref') is False for f in files):
            plasmid_name = trim_benchling_search(folder)
            result = benchling_cached_fetch(plasmid_name)
            full_name = result['name']
            seq = result['bases']
            file_name = "ref_"+full_name+".seq"
            with open(os.path.join(os.path.join("./samples", folder), file_name), 'w') as f:
                f.write(seq)
                f.close()

def align_and_visualize(sample):
    print("\nProcessing sample", sample)
    sample_dir = os.path.join("./samples", sample)

    files = os.listdir(sample_dir)
    ab1_files = []
    aligned_sequences = []
    ref_seq = ''
    for f in files:
        if f.endswith('.ab1'):
            record = SeqIO.read(os.path.join(sample_dir, f), 'abi')
            length = len(str(record.seq))
            if length > 30:
                ab1_files.append(os.path.join(sample_dir, f)[2:])
                aligned_sequences.append(str(record.seq))
        elif f.startswith('ref'):
            with open(os.path.join(sample_dir, f), 'r') as x:
                ref_seq = x.read()
                x.close()
    if ref_seq == '':
        return
    msa = mafft_align(ref_seq, ab1_files, trim=False)

    # fetch data out of MSA
    sequence_strs = [str(m.seq) for m in msa[1:]]
    ref = str(msa[0].seq)

    ## Find the longest common substring between each aligned file and reference
    if not os.path.exists(os.path.join(sample_dir, "coverage_report.txt")):
        covered = []

        for x in tqdm(sequence_strs):
            match = longest_common_substring(x.lower(), ref.lower())
            start = x.find(match)
            end = start + len(match)
            covered.append((start, end))

        c = collapse(covered)

        f = open(os.path.join(sample_dir, "coverage_report.txt"), 'w')
        f.write("Pre collapse\n")
        f.write(str(covered))
        f.write("Collapsed\n")
        f.write(str(c))

        # TODO make automatic
        Kan_length = 816
        #Spec_length = 786
        try:
            (a, b) = list(c)[0]
            if a < Kan_length/2 and b > len(ref_seq)-Kan_length/2:
                print("Perfect colony auto-detected", sample)
                f.write("Perfect colony auto-detected\n")
        except:
            print("Autodetect not avaliable")
            f.write("Autodetect not avaliable\n")
            f.close()
            return

        f.close()

    ## VISUALIZE
    results = sequence_strs

    visualized = [[] for x in range(len(results))]
    for i in range(len(ref)):
        r = ref[i]
        for j in range(len(results)):
            if results[j][i] == r and r != '-':
                visualized[j] += [1]    # match
            elif results[j][i] == r and r == '-':
                visualized[j] += [.75]    # matching -
            elif results[j][i] == '-':
                visualized[j] += [0.5]  #read does not cover this section
            else:
                visualized[j] += [0]    # mismatch

    y = np.array([np.array(xi) for xi in visualized])

    fig, ax = plt.subplots(1, 1)
    plt.title(sample)
    plt.imshow(y, aspect=30)

    plt.yticks(np.arange(len(results)), tuple([str(m.name) for m in msa[1:]]), fontsize=2)

    plt.tight_layout()
    plt.savefig(os.path.join(sample_dir, sample+".png"), dpi=500, bbox_inches='tight')





#clean_up_folders()
#extract_new_files()
#fetch_from_benchling()#prefetch=True)
#arrange_benchling_files()

sample_folder_names = [x for x in os.listdir("./samples") if x.startswith('.') is False]
for sample in sample_folder_names:
    try:
        align_and_visualize(sample)
    except Exception as e:
        print("Exception ", str(e), " for sample ", sample)
