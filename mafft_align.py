from Bio import SeqIO
from Bio import AlignIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from io import StringIO, BytesIO
import numpy as np
import tempfile
import subprocess
import os
import pdb

def read_ab1(ab1_file, array_wrapper=np.array):
    seq = SeqIO.read(ab1_file, 'abi')
    quality = seq.letter_annotations['phred_quality']
    peak_calls = seq.annotations['abif_raw']['PLOC2']
    order = seq.annotations['abif_raw']['FWO_1']
    sequence = str(seq.seq)
    # Extract color data
    peaks = {}
    for i, (base, r) in enumerate(zip(order, [9, 10, 11, 12])):
        peaks[base] = array_wrapper(seq.annotations['abif_raw']['DATA{}'.format(r)])
    return {'name': seq.id, 'sequence': sequence, 'peaks': peaks, 'peak_calls': array_wrapper(peak_calls), 'quality': array_wrapper(quality)}

def write_fasta(seqs, handle):
    #records = [SeqRecord(Seq(seq, IUPAC.DNA), name) for name, seq in seqs.items()]
    records = [SeqRecord(Seq(seq), name) for name, seq in seqs.items()]
    SeqIO.write(records, handle, 'fasta')
    
def align_mafft(seqs):
    with tempfile.NamedTemporaryFile() as input_handle:
        write_fasta(seqs, input_handle.name) # TODO: write directly using file handle
        output_data = call_subprocess(['mafft', '--quiet', '--adjustdirection', "--globalpair", input_handle.name])
        return next(AlignIO.parse(StringIO(output_data.decode('utf-8')), 'fasta'))#, IUPAC.DNA)

def call_subprocess(args, input_data=None):
    if input_data is not None:
        stdin = subprocess.PIPE
    else:
        stdin = None
    proc = subprocess.Popen(args, stdin=stdin, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    if input_data is not None:
        input_bytes = input_data.encode('utf-8')
    else:
        input_bytes = None
    stdout, stderr = proc.communicate(input_bytes)
    if stderr:
        raise Exception('got stderr: "{}"'.format(stderr))
    return stdout
#     if stdin is not None:
#         proc.stdin.write(input_data.encode('utf-8'))
#         proc.stdin.close()
#     while proc.returncode is None:
#         proc.poll()
#     return proc.stdout.read()

def trim_to_ref(msa, ref_name='ref'):
    ary = np.array([list(str(s.seq)) for s in msa if s.description != ref_name])
    mask = (ary == '-').sum(axis=0) >= len(msa) - 1
    idx_start = (~mask).argmax()
    idx_stop = (~mask)[::-1].argmax()
    return msa[:,idx_start:idx_stop]
    
def mafft_align(ref_seq, ab1_files, trim=True):
    ab1s = {os.path.basename(ab1_file): read_ab1(ab1_file) for ab1_file in ab1_files}
    seqs = {name: ab1['sequence'] for name, ab1 in ab1s.items()}
    seqs = {'ref': ref_seq, **seqs}
    msa = align_mafft(seqs)    
    if(trim):
        msa = trim_to_ref(msa)
    return msa
    
# example usage:
#ref_seq = "tctcatctgtaacatcattggcaacgctacctttgccatgtttcagaaacaactctggcgcatcgggcttcccatacaatcgatagattgtcgcacctgattgcccgacattatcgcgagcccatttatacccatataaatcagcatccatgttggaatttaatcgcggcctcgagcaagacgtttcccgttgaatatggctcataacaccccttgtattactgtttatgtaagcagacagttttattgttcatgatgatatatttttatcttgtgcaatgtaacatcagagattttgaaggccaaataggccgttcagatccttccgtatttagccagtatgttctctagtgtggttcgttgtttttgcgtgagccatgagaacgaaccattgagatcatgcttactttgcatgtcactcaaaaattttgcctcaaaactggtgagctgaatttttgcagttaaagcatcgtgtagtgtttttcttagtccgttacgtaggtaggaatctgatgtaatggttgttggtattttgtcaccattcatttttatctggttgttctcaagttcggttacgagatccatttgtctatctagttcaacttggaaaatcaacgtatcagtcgggcggcctcgcttatcaaccaccaatttcatattgctgtaagtgtttaaatctttacttattggtttcaaaacccattggttaagccttttaaactcatggtagttattttcaagcattaacatgaacttaaattcatcaaggctaatctctatatttgccttgtgagttttcttttgtgttagttcttttaataaccactcataaatcctcatagagtatttgttttcaaaagacttaacatgttccagattatattttatgaatttttttaactggaaaagataaggcaatatctcttcactaaaaactaattctaatttttcgcttgagaacttggcatagtttgtccactggaaaatctcaaagcctttaaccaaaggattcctgatttccacagttctcgtcatcagctctctggttgctttagctaatacaccataagcattttccctactgatgttcatcatctgagcgtattggttataagtgaacgataccgtccgttctttccttgtagggttttcaatcgtggggttgagtagtgccacacagcataaaattagcttggtttcatgctccgttaagtcatagcgactaatcgctagttcatttgctttgaaaacaactaattcagacatacatctcaattggtctaggtgattttaatcactataccaattgagatgggctagtcaatgataattactagtccttttcctttgagttgtgggtatctgtaaattctgctagacctttgctggaaaacttgtaaattctgctagaccctctgtaaattccgctagacctttgtgtgttttttttgtttatattcaagtggttataatttatagaataaagaaagaataaaaaaagataaaaagaatagatcccagccctgtgtataactcactactttagtcagttccgcagtattacaaaaggatgtcgcaaacgctgtttgctcctctacaaaacagaccttaaaaccctaaaggcttaagtagcaccctcgcaagctcgggcaaatcgctgaatattccttttgtctccgaccatcaggcacctgagtcgctgtctttttcgtgacattcagttcgctgcgctcacggctctggcagtgaatgggggtaaatggcactacaggcgccttttatggattcatgcaaggaaactacccataatacaagaaaagcccgtcacgggcttctcagggcgttttatggcgggtctgctatgtggtgctatctgactttttgctgttcagcagttcctgccctctgattttccagtctgaccacttcggattatcccgtgacaggtcattcagactggctaatgcacccagtaaggcagcggtatcatcaacaggcttacccgtcttactgtcAAGAGGACATCCGGTttgttcagaacgctcggtcttgcacaccgggcgttttttctttgtgagtccatagtacacttgaataaataaaaacagccgttgccagaaagaggcacggctgtttttattttagacttagggaccctcacagctaacaccacgtcgtccctatctgctgccctaggtctatgagtggttgctggataactttacgggcatgcataaggctcgtataatatattcagggagaccacaacggtttccctctacaaataattttgtttaactttatatctgttattttttcatatacaagatctatgaaaaaattattattcgcaattcctttagttgttcctttctattctcactccgctgaaactgttgaaagttgtttagcaaaaCCCcatacagaaaattcatttactaacgtctggaaagacgacaaaactttagatcgttacgctaactatgagggctgtctgtggaatgctacaggcgttgtagtttgtactggtgacgaaactcagtgttacggtacatgggttcctattgggcttgctatccctgaaaatgagggtggtggctctgagggtggtggttctgagggtggcggttctgagggtggcggtactaaacctcctgagtacggtgatacacctattccgggctatacttatatcaaccctctcgacggcacttatccgcctggtactgagcaaaaccccgctaatcctaatccttctcttgaggagtctcagcctcttaatactttcatgtttcagaataataggttccgaaataggcagggggcattaactgtttatacgggcactgttactcaaggcactgaccccgttaaaacttattaccagtacactcctgtatcatcaaaagccatgtatgacgcttactggaacggtaaattcagagactgcgctttccattctggctttaatgaggatccattcgtttgtgaatatcaaggccaatcgtctgacctgcctcaacctcctgtcaatgctggcggcggctctggtggtggttctggtggcggctctgagggtggtggctctgagggtggcggttctgagggtggcggctctgagggaggcggttccggtggtggctctggttccggtgattttgattatgaaaagatggcaaacgctaataagggggctatgaccgaaaatgccgatgaaaacgcgctacagtctgacgctaaaggcaaacttgattctgtcgctactgattacggtgctgctatcgatggtttcattggtgacgtttccggccttgctaatggtaatggtgctactggtgattttgctggctctaattcccaaatggctcaagtcggtgacggtgataattcacctttaatgaataatttccgtcaatatttaccttccctccctcaatcggttgaatgtcgcccttttgtctttggcgctggtaaaccttacgagttcagtatcgactgcgataagatcaacctgttccgcggtgtctttgcgtttcttttatatgttgccacctttatgtatgtattttctacgtttgctaacatactgcgtaataaggagtcttaaacttaattaacggcactcctcagccaagtcaaaagcctccgaccggaggcttttgactacatgcccatggcgtttagaaaaactcatcgagcatcaaatgaaactgcaatttattcatatcaggattatcaataccatatttttgaaaaagccgtttctgtaatgaaggagaaaactcaccgaggcagttccataggatggcaagatcctggtatcggtctgcgattccgactcgtccaacatcaatacaacctattaatttcccctcgtcaaaaataaggttatcaagtgagaaatcaccatgagtgacgactgaatccggtgagaatggcaaaagcttatgcatttctttccagacttgttcaacaggccagccattacgctcgtcatcaaaatcactcgcatcaaccaaaccgttattcattcgtgattgcgcctgagcgagacgaaatacgcgatcgctgttaaaaggacaattacaaacaggaatcgaatgcaaccggcgcaggaacactgccagcgcatcaacaatattttcacctgaatcaggatattcttctaatacctggaatgctgttttcccggggatcgcagtggtgagtaaccatgcatcatcaggagtacggataaaatgcttgatggtcggaagaggcataaattccgtcagccagtttagtctgacca"      
#ab1_files = ['testseq/1.ab1', 'testseq/2.ab1', 'testseq/3.ab1', 'testseq/4.ab1']

#mafft_align(ref_seq, ab1_files)