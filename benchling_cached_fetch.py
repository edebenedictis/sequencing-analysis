from benchlingapi import BenchlingAPI
import os.path
import pickle

def benchling_internet_fetch(query):
    '''Returns a name, bases tuple containing the sequence of the first query result.
    If a supported antibiotic resistance marker is annotated in the plasmid, the middle of
    the antibiotic marker is reindexed to zero
    '''
    print("Fetching from Benchling " + query)
    # read locally stored api key
    with open('api_key.txt') as f:
        content = f.readlines()
    bench_api_key = [x.strip() for x in content][0]
    benchlingapi = BenchlingAPI(bench_api_key)

    # query benchling and take first result
    result = benchlingapi.search(query)
    first_result = result['results'][0]
    seq = benchlingapi.get_sequence(first_result['id'])
    return (seq)
   
def benchling_cached_fetch(query):
    '''If a local copy exists, return.
    Otherwise fetch, save a local copy, and return.'''
    
    if not os.path.isdir("benchling_cache"):
        os.mkdir("benchling_cache")
    
    pkl_file = "benchling_cache/" + query + ".pkl"
    
    if (os.path.isfile(pkl_file)):
        reader = pickle.load(open(pkl_file, 'rb'))
        return reader
    
    else:
        result = benchling_internet_fetch(query)
        pickle.dump(result, open(pkl_file,'wb'))
        return result

#x = benchling_cached_fetch('tRNA-Ala-GGC-1-1 E coli')
#print(x)